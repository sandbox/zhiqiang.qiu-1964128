<?php

/**
 * @file
 * Provide views data and handlers for drupal_booking.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function drupalbooking_unit_views_data() {

  // Define the base group of this table. Fields that don't have a group defined
  // will go into this field by default.
  $data['drupalbooking_unit']['table']['group']  = t('Booking Unit');

  $data['drupalbooking_unit']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Booking Unit'),
    'help' => t('Booking Unit Label.'),
		'defaults' => array(
      'field' => 'name'
    ),
  );
  $data['drupalbooking_unit']['table']['entity type'] = 'drupalbooking_unit';
  $data['drupalbooking_unit']['table']['wizard_id'] = 'drupalbooking_unit';

  $data['drupalbooking_unit']['id'] = array(
    'title' => t('Id'),
    'help' => t('The Booking Unit ID'),
    'field' => array(
      'id' => 'unit_id',
    ),
    'argument' => array(
      'id' => 'unit_id',
      'name field' => 'id',
    ),
    'filter' => array(
      'title' => t('Name'),
      'id' => 'name',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['drupalbooking_unit']['name'] = array(
    'title' => t('Name'),
    'help' => t('The user or author name.'),
    'field' => array(
      'id' => 'unit_name',
      'link_to_unit default' => TRUE,
     ),
    'sort' => array(
      'id' => 'standard',
    ),
    'argument' => array(
      'id' => 'string',
    ),
    'filter' => array(
      'id' => 'string',
      'title' => t('Name (raw)'),
      'help' => t('The user or author name. This filter does not check if the user exists and allows partial matching. Does not utilize autocomplete.')
    ),
  );

  $data['drupalbooking_unit']['langcode'] = array(
    'title' => t('Language'),
    'help' => t('Language of the user'),
    'field' => array(
      'id' => 'unit_language',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'language',
    ),
    'argument' => array(
      'id' => 'language',
    ),
  );

  $data['drupalbooking_unit']['type'] = array(
    'title' => t('Unit Type'),
    'help' => t('The type of this drupalbooking_unit.'),
    'field' => array(
      'id' => 'unit_type',
    ),
    'sort' => array(
      'id' => 'unit_type'
    ),
    'filter' => array(
      'id' => 'unit_type',
    ),
  );

  return $data;
}
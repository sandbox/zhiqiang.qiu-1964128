<?php

/**
 * @file
 * Admin page callbacks for the drupalbooking_unit module.
 */

use Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType;

/**
 * Page callback: Lists drupalbooking_unit types.
 *
 * @return array
 *   A build array in the format expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_type_list() {
  return drupal_container()->get('plugin.manager.entity')->getListController('drupalbooking_unit_type')->render();
}

/**
 * Page callback: Presents the drupalbooking_unit type creation form.
 *
 * @return array
 *   A form array as expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_type_add() {
  $unit_type = entity_create('drupalbooking_unit_type', array());
  return entity_get_form($unit_type);
}

/**
 * Page callback: Presents the drupalbooking_unit type edit form.
 *
 * @param \Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType $unit_type
 *   The drupalbooking_unit type to edit.
 *
 * @return array
 *   A form array as expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_type_edit(DrupalBookingUnitType $unit_type) {
  return entity_get_form($unit_type);
}

/**
 * Page callback: Form constructor for the drupalbooking_unit type deletion form.
 *
 * @param \Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType $unit_type
 *   The drupalbooking_unit type to be deleted.
 *
 * @see drupalbooking_unit_menu()
 * @see drupalbooking_unit_type_delete_form_submit()
 *
 * @ingroup forms
 */
function drupalbooking_unit_type_delete_form($form, &$form_state, DrupalBookingUnitType $unit_type) {
  $form_state['drupalbooking_unit_type'] = $unit_type;
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $unit_type->id(),
  );

  $message = t('Are you sure you want to delete %label?', array('%label' => $unit_type->label()));

  $blocks = entity_query('drupalbooking_unit')->condition('type', $unit_type->id())->execute();
  if (!empty($blocks)) {
    drupal_set_title($message, PASS_THROUGH);
    $caption = '<p>' . format_plural(count($blocks), '%label is used by 1 drupalbooking_unit on your site. You can not remove this bookable unit type until you have removed all of the %label blocks.', '%label is used by @count drupalbooking_units on your site. You may not remove %label until you have removed all of the %label drupalbooking_units.', array('%label' => $unit_type->label())) . '</p>';
    $form['description'] = array('#markup' => $caption);
    return $form;
  }

  return confirm_form(
    $form,
    $message,
    'admin/drupalbooking/unit-types',
    t('This action cannot be undone.'),
    t('Delete')
  );
}

/**
 * Form submission handler for drupalbooking_unit_type_delete_form().
 */
function drupalbooking_unit_type_delete_form_submit($form, &$form_state) {
  $unit_type = $form_state['drupalbooking_unit_type'];
  $unit_type->delete();

  drupal_set_message(t('Drupal booking unit type %label has been deleted.', array('%label' => $unit_type->label())));
  watchdog('drupalbooking_unit', 'Drupal booking unit type %label has been deleted.', array('%label' => $unit_type->label()), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/drupalbooking/unit-types';
}

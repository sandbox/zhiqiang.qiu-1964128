<?php

/**
 * @file
 * Admin page callbacks for the drupalbooking_unit module.
 */

use Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType;
use Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnit;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Page callback: Lists drupalbooking_unit.
 *
 * @return array
 *   A build array in the format expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_admin($callback_arg = '') {
	$op = isset($_POST['op']) ? $_POST['op'] : $callback_arg;

	switch ($op) {
    case 'edit':
    case 'delete':
      $unit = entity_create('drupalbooking_unit', array());
      $build['unit_edit'] = entity_get_form($unit, 'edit');
      break;
    default:
      if (!empty($_POST['unit']) && isset($_POST['operation']) && ($_POST['operation'] == 'cancel')) {
        $build['unit_cancel_confirm'] = drupal_get_form('drupalbooking_unit_cancel_confirm');
      }
      else {
        $build['unit_admin'] = drupal_get_form('drupalbooking_unit_admin_list');
      }
  }
	
  return $build;
}

/**
 * Form builder; User administration page.
 *
 * @ingroup forms
 * @see drupalbooking_unit_admin_list_validate()
 * @see drupalbooking_unit_admin_list_submit()
 */
function drupalbooking_unit_admin_list() {

  $header = array(
    'id' => array('data' => t('ID'), 'field' => 'du.id'),
    'type' => array('data' => t('Boeking Unit Type'), 'field' => 'du.type', 'class' => array(RESPONSIVE_PRIORITY_LOW)),
    'name' => array('data' => t('Name'),  'field' => 'du.name', 'class' => array(RESPONSIVE_PRIORITY_LOW)),
    'base_price' => array('data' => t('Base Price'), 'sort' => 'desc', 'class' => array(RESPONSIVE_PRIORITY_LOW)),
    'link' => array('data' => t('Link'),'class' => array(RESPONSIVE_PRIORITY_LOW)),
    'operations' => t('Operations links'),
  );

  $query = db_select('drupalbooking_unit', 'du');
  user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(du.id)');

  $query = $query
    ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
    ->extend('Drupal\Core\Database\Query\TableSortExtender');
  $query
    ->fields('du', array('id', 'name', 'type'))
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  $options = array();

  $destination = drupal_get_destination();
  $status = array(t('blocked'), t('active'));
  $roles = array_map('check_plain', user_role_names(TRUE));
  $units = array();
  foreach ($result as $unit) {
    $options[$unit->id] = array(
      'id' => $unit->id,
      'type' =>  $unit->type,
      'name' => $unit->name,
      'base_price' => $unit->id,      
    );
    $links = array();
    $links['edit'] = array(
      'title' => t('Edit'),
      'href' => 'admin/drupalbooking/units/' . $unit->id . '/edit',
      'query' => $destination,
    );
    if (module_invoke('translation_entity', 'translate_access', $unit)) {
      $links['translate'] = array(
        'title' => t('Translate'),
        'href' => 'admin/drupalbooking/units/' . $unit->id . '/translations',
        'query' => $destination,
      );
    }
    $options[$unit->id]['operations']['data'] = array(
      '#type' => 'operations',
      '#links' => $links,
    );
  }

  $form['units'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No people available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Page callback: Displays add custom unit links for available types.
 *
 * @return array
 *   A render array for a list of the custom unit types that can be added or
 *   if there is only one custom unit type defined for the site, the function
 *   returns the custom unit add page for that custom unit type.
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_add_page() {
  $types = entity_load_multiple('drupalbooking_unit_type');
  if ($types && count($types) == 1) {
    $type = reset($types);
    return drupalbooking_unit_add($type);
  }

  return array('#theme' => 'drupalbooking_unit_add_list', '#content' => $types);
}

/**
 * Returns HTML for a list of available custom unit types for unit creation.
 *
 * @param $variables
 *   An associative array containing:
 *   - content: An array of unit types.
 *
 * @see drupalbooking_unit_add_page()
 *
 * @ingroup themeable
 */
function theme_drupalbooking_unit_add_list($variables) {
  $content = $variables['content'];
  $output = '';

  if ($content) {
    $output = '<dl class="node-type-list">';
    foreach ($content as $type) {
      $output .= '<dt>' . l($type->label(), 'admin/drupalbooking/units/add/' . $type->id()) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($type->description) . '</dd>';
    }
    $output .= '</dl>';
  }
  return $output;
}


/**
 * Page callback: Presents the custom unit creation form.
 *
 * @param Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType $unit_type
 *   The custom unit type to add.
 *
 * @return array
 *   A form array as expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_add(DrupalBookingUnitType $unit_type) {
  drupal_set_title(t('Add %type custom unit', array(
    '%type' => $unit_type->label()
  )), PASS_THROUGH);
  $unit = entity_create('drupalbooking_unit', array(
    'type' => $unit_type->id()
  ));

  return entity_get_form($unit);
}

/**
 * Page callback: Presents the custom unit edit form.
 *
 * @param Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnit $unit
 *   The custom unit to edit.
 *
 * @return array
 *   A form array as expected by drupal_render().
 *
 * @see drupalbooking_unit_menu()
 */
function drupalbooking_unit_edit(DrupalBookingUnit $unit) {
  drupal_set_title(t('Edit custom unit %label', array('%label' => $unit->label())), PASS_THROUGH);
  return entity_get_form($unit);
}

/**
 * Page callback: Form constructor for the custom unit deletion form.
 *
 * @param Drupal\drupalbooking_unit\Plugin\Core\Entity\CustomBlock $unit
 *   The custom unit to be deleted.
 *
 * @see drupalbooking_unit_menu()
 * @see drupalbooking_unit_delete_form_submit()
 *
 * @ingroup forms
 */
function drupalbooking_unit_delete_form($form, &$form_state, CustomBlock $unit) {
  $form_state['drupalbooking_unit'] = $unit;
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $unit->id(),
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %label?', array('%label' => $unit->label())),
    'admin/structure/unit',
    t('This action cannot be undone.'),
    t('Delete')
  );
}

/**
 * Form submission handler for drupalbooking_unit_delete_form().
 */
function drupalbooking_unit_delete_form_submit($form, &$form_state) {
  // @todo Delete all configured instances of the unit.
  $unit = $form_state['drupalbooking_unit'];
  $unit->delete();

  drupal_set_message(t('Custom unit %label has been deleted.', array('%label' => $unit->label())));
  watchdog('drupalbooking_unit', 'Custom unit %label has been deleted.', array('%label' => $unit->label()), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/structure/unit';
}
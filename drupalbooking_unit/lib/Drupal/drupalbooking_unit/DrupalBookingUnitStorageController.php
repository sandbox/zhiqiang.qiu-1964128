<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\DrupalBookingUnitStorageController.
 */

namespace Drupal\drupalbooking_unit;

use Drupal\Core\Entity\DatabaseStorageControllerNG;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller class for drupalbooking_units.
 *
 * This extends the Drupal\Core\Entity\DatabaseStorageController class, adding
 * required special handling for drupalbooking_unit entities.
 */
class DrupalBookingUnitStorageController extends DatabaseStorageControllerNG {

  /**
   * Overrides \Drupal\Core\Entity\DatabaseStorageController::preSaveRevision().
   */
  protected function preSaveRevision(\stdClass $record, EntityInterface $entity) {
    if ($entity->isNewRevision()) {
      // When inserting either a new drupalbooking_unit or a new drupalbooking_unit
      // revision, $entity->log must be set because {block_custom_revision}.log
      // is a text column and therefore cannot have a default value. However,
      // it might not be set at this point (for example, if the user submitting
      // the form does not have permission to create revisions), so we ensure
      // that it is at least an empty string in that case.
      // @todo: Make the {block_custom_revision}.log column nullable so that we
      // can remove this check.
      if (!isset($record->log)) {
        $record->log = '';
      }
    }
    elseif (!isset($record->log) || $record->log === '') {
      // If we are updating an existing drupalbooking_unit without adding a new
      // revision, we need to make sure $entity->log is unset whenever it is
      // empty. As long as $entity->log is unset, drupal_write_record() will not
      // attempt to update the existing database column when re-saving the
      // revision; therefore, this code allows us to avoid clobbering an
      // existing log entry with an empty one.
      unset($record->log);
    }
  }

  /**
   * Overrides \Drupal\Core\Entity\DatabaseStorageController::attachLoad().
   */
  protected function attachLoad(&$units, $load_revision = FALSE) {
    // Create an array of bookable unit types for passing as a load argument.
    // Note that units at this point are still \StdClass objects returned from
    // the database.
    foreach ($units as $id => $entity) {
      $types[$entity->type] = $entity->type;
    }

    // Besides the list of units, pass one additional argument to
    // hook_drupalbooking_unit_load(), containing a list of bookable unit types that were
    // loaded.
    $this->hookLoadArguments = array($types);
    parent::attachLoad($units, $load_revision);
  }

  /**
   * Overrides \Drupal\Core\Entity\DatabaseStorageController::postSave().
   */
  protected function postSave(EntityInterface $unit, $update) {
    // Invalidate the unit cache to update drupalbooking_unit-based derivatives.
    drupal_container()->get('plugin.manager.unit')->clearCachedDefinitions();
  }

  /**
   * Implements \Drupal\Core\Entity\DataBaseStorageControllerNG::basePropertyDefinitions().
   */
  public function baseFieldDefinitions() {
    $properties['id'] = array(
      'label' => t('ID'),
      'description' => t('The drupalbooking_unit ID.'),
      'type' => 'integer_field',
      'read-only' => TRUE,
    );
    $properties['uuid'] = array(
      'label' => t('UUID'),
      'description' => t('The drupalbooking_unit UUID.'),
      'type' => 'string_field',
    );
    $properties['revision_id'] = array(
      'label' => t('Revision ID'),
      'description' => t('The revision ID.'),
      'type' => 'integer_field',
    );
    $properties['langcode'] = array(
      'label' => t('Language code'),
      'description' => t('The comment language code.'),
      'type' => 'language_field',
    );
    $properties['name'] = array(
      'label' => t('Name'),
      'description' => t('The drupalbooking_unit name.'),
      'type' => 'string_field',
    );
    $properties['type'] = array(
      'label' => t('Unit type'),
      'description' => t('The bookable unit type.'),
      'type' => 'string_field',
    );
    $properties['log'] = array(
      'label' => t('Revision log message'),
      'description' => t('The revision log message.'),
      'type' => 'string_field',
    );
    return $properties;
  }

}

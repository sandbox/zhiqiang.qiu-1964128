<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\DrupalBookingUnitTypeStorageController.
 */

namespace Drupal\drupalbooking_unit;

use Drupal\Core\Config\Entity\ConfigStorageController;
use Drupal\Core\Entity\EntityInterface;

/**
 * Controller class for drupalbooking_unit types.
 */
class DrupalBookingUnitTypeStorageController extends ConfigStorageController {

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigStorageController::postSave().
   */
  protected function postSave(EntityInterface $entity, $update) {
    parent::postSave($entity, $update);

    if (!$update) {
      field_attach_create_bundle('drupalbooking_unit', $entity->id());
      drupalbooking_unit_add_body_field($entity->id());
    }
    elseif ($entity->original->id() != $entity->id()) {
      field_attach_rename_bundle('drupalbooking_unit', $entity->original->id(), $entity->id());
    }
  }

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigStorageController::postDelete().
   */
  protected function postDelete($entities) {
    parent::postDelete($entities);

    foreach ($entities as $entity) {
      field_attach_delete_bundle('drupalbooking_unit', $entity->id());
    }
  }

}

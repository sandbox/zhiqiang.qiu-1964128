<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\wizard\DrupalbookingUnit.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\wizard;

use Drupal\views\Plugin\views\wizard\WizardPluginBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Tests creating drupalbooking_unit views with the wizard.
 *
 * @Plugin(
 *   id = "drupalbooking_unit",
 *   module = "drupalbooking_unit",
 *   base_table = "drupalbooking_unit",
 *   title = @Translation("Booking Unit")
 * )
 */
class DrupalbookingUnit extends WizardPluginBase {

  /**
   * Set default values for the path field options.
   */
  protected $pathField = array(
    'id' => 'id',
    'table' => 'drupalbooking_unit',
    'field' => 'id',
    'exclude' => TRUE,
		'link_to_unit' => FALSE,
    'alter' => array(
      'alter_text' => TRUE,
      'text' => 'unit/[tid]'
    )
  );

  /**
   * Overrides Drupal\views\Plugin\views\wizard\WizardPluginBase::getAvailableSorts().
   *
   * @return array
   */
  public function getAvailableSorts() {
    // You can't execute functions in properties, so override the method
    return array(
      'name:DESC' => t('Name')
    );
  }

  /**
   * Overrides Drupal\views\Plugin\views\wizard\WizardPluginBase::default_display_options().
   */
  protected function default_display_options() {
    $display_options = parent::default_display_options();

		// Add permission-based access control.
    $display_options['access']['type'] = 'perm';

    // Remove the default fields, since we are customizing them here.
    unset($display_options['fields']);

    /* Field: File: Name */
    $display_options['fields']['name']['id'] = 'unit_name';
    $display_options['fields']['name']['table'] = 'drupalbooking_unit';
    $display_options['fields']['name']['field'] = 'name';
    $display_options['fields']['name']['label'] = 'Name';
    $display_options['fields']['name']['alter']['alter_text'] = 0;
    $display_options['fields']['name']['alter']['make_link'] = 0;
    $display_options['fields']['name']['alter']['absolute'] = 0;
    $display_options['fields']['name']['alter']['trim'] = 0;
    $display_options['fields']['name']['alter']['word_boundary'] = 0;
    $display_options['fields']['name']['alter']['ellipsis'] = 0;
    $display_options['fields']['name']['alter']['strip_tags'] = 0;
    $display_options['fields']['name']['alter']['html'] = 0;
    $display_options['fields']['name']['hide_empty'] = 0;
    $display_options['fields']['name']['empty_zero'] = 0;
    $display_options['fields']['name']['link_to_unit'] = 1;

    return $display_options;
  }

}

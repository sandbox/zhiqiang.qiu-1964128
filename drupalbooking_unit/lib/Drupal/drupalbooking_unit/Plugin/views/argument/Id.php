<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\argument\Id.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\argument;

use Drupal\Component\Annotation\Plugin;
use Drupal\views\Plugin\views\argument\Numeric;

/**
 * Argument handler to accept multiple file ids.
 *
 * @ingroup views_argument_handlers
 *
 * @Plugin(
 *   id = "unit_id",
 *   module = "drupalbooking_unit"
 * )
 */
class Id extends Numeric {

  /**
   * Override the behavior of title_query(). Get the filenames.
   */
  function title_query() {
    $unit_ids = db_select('drupalbooking_unit', 'du')
      ->fields('du', array('id'))
      ->condition('id', $this->value)
      ->execute()
      ->fetchCol();
    foreach ($unit_ids as &$unit_id) {
      $unit_id = check_plain($unit_id);
    }
    return $unit_id;
  }

}

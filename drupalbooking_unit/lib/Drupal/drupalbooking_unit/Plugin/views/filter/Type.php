<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\filter\Type.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Component\Annotation\Plugin;

/**
 * Filter by file status.
 *
 * @ingroup views_filter_handlers
 *
 * @Plugin(
 *   id = "unit_type",
 *   module = "drupalbooking_unit"
 * )
 */
class Type extends FilterPluginBase {

  function get_value_options() {
    $options = parent::defineOptions();print_r($options);die();

    $options['type'] = array('default' => 'textfield');
    $options['limit'] = array('default' => TRUE, 'bool' => TRUE);
    $options['vid'] = array('default' => '');
    $options['hierarchy'] = array('default' => 0);
    $options['error_message'] = array('default' => TRUE, 'bool' => TRUE);

    return $options;
  }

}

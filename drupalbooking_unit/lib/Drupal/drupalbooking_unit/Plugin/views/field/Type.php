<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\field\Type.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\field;

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Component\Annotation\Plugin;

/**
 * Field handler to allow linking to a name.
 *
 * @ingroup views_field_handlers
 *
 * @Plugin(
 *   id = "unit_type",
 *   module = "drupalbooking_unit"
 * )
 */
class Type extends FieldPluginBase {

  function render($values) {
    $value = $this->get_value($values);
    return $value;
  }

}

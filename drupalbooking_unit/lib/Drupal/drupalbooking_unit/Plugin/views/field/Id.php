<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\field\Id.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\field;

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Field handler to provide simple renderer that allows linking to a drupalbooking_unit.
 *
 * @ingroup views_field_handlers
 *
 * @Plugin(
 *   id = "unit_id",
 *   module = "drupalbooking_unit"
 * )
 */
class Id extends FieldPluginBase {

  /**
   * Overrides \Drupal\views\Plugin\views\field\FieldPluginBase::init().
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    if (!empty($this->options['link_to_unit'])) {
      $this->additional_fields['id'] = 'id';
    }
  }

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['link_to_unit'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to file option
   */
  public function buildOptionsForm(&$form, &$form_state) {
    $form['link_to_unit'] = array(
      '#title' => t('Link this field to its unit'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_unit']),
    );
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Render whatever the data is as a link to the file.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_unit'])) {
      $this->options['alter']['make_link'] = TRUE;
			$id = $this->get_value($values, 'id');
      $this->options['alter']['path'] = "unit/" . $id;
    }

    return $data;
  }

  function render($values) {
    $value = $this->get_value($values);
    return $this->render_link($this->sanitizeValue($value), $values);
  }

}

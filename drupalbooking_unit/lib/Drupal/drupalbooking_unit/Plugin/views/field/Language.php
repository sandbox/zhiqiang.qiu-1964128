<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\field\Language.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\field;

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Component\Annotation\Plugin;

/**
 * Field handler to allow linking to a Language.
 *
 * @ingroup views_field_handlers
 *
 * @Plugin(
 *   id = "unit_language",
 *   module = "drupalbooking_unit"
 * )
 */
class Language extends FieldPluginBase {

  /**
   * Work out the depth of this Language
   */
  function render($values) {
    $value = $this->get_value($values);
    return $value;
  }

}

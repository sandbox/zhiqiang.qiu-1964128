<?php

/**
 * @file
 * Definition of Drupal\drupalbooking_unit\Plugin\views\field\Name.
 */

namespace Drupal\drupalbooking_unit\Plugin\views\field;

use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Component\Annotation\Plugin;

/**
 * Field handler to allow linking to a name.
 *
 * @ingroup views_field_handlers
 *
 * @Plugin(
 *   id = "unit_name",
 *   module = "drupalbooking_unit"
 * )
 */
class Name extends FieldPluginBase {

  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['link_to_unit'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  public function buildOptionsForm(&$form, &$form_state) {    
    $form['link_to_unit'] = array(
      '#title' => t('Link this field to its unit.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['link_to_unit'],
    );
    parent::buildOptionsForm($form, $form_state);
  }

  function render($values) {
    $unit_name = $this->get_value($values);
    return $this->render_link($unit_name, $values);
  }

  function render_link($data, $values) {
    $unit = $values;
    $id = $unit->id;

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['html'] = TRUE;

		if($this->options['link_to_unit']) {
			$this->options['alter']['path'] = "unit/" . $id;
		}

    return $data;
  }

}

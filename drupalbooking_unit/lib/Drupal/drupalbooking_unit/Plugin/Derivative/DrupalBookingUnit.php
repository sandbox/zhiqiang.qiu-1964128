<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\Plugin\Derivative\DrupalBookingUnit.
 */

namespace Drupal\drupalbooking_unit\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DerivativeInterface;

/**
 * Retrieves block plugin definitions for all drupalbooking_units.
 */
class DrupalBookingUnit implements DerivativeInterface {

  /**
   * List of derivative definitions.
   *
   * @var array
   */
  protected $derivatives = array();

  /**
   * Implements \Drupal\Component\Plugin\Derivative\DerivativeInterface::getDerivativeDefinition().
   *
   * Retrieves a specific drupalbooking_unit definition from storage.
   */
  public function getDerivativeDefinition($derivative_id, array $base_plugin_definition) {
    if (!empty($this->derivatives) && !empty($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
    $this->getDerivativeDefinitions($base_plugin_definition);
    return $this->derivatives[$derivative_id];
  }

  /**
   * Implements \Drupal\Component\Plugin\Derivative\DerivativeInterface::getDerivativeDefinitions().
   *
   * Retrieves drupalbooking_unit definitions from storage.
   */
  public function getDerivativeDefinitions(array $base_plugin_definition) {
    $drupalbooking_units = entity_load_multiple('drupalbooking_unit');
    foreach ($drupalbooking_units as $drupalbooking_unit) {
      $this->derivatives[$drupalbooking_unit->uuid->value] = $base_plugin_definition;
      $this->derivatives[$drupalbooking_unit->uuid->value]['settings'] = array(
        'name' => $drupalbooking_unit->name->value,
      ) + $base_plugin_definition['settings'];
      $this->derivatives[$drupalbooking_unit->uuid->value]['admin_label'] = $drupalbooking_unit->name->value;
    }
    return $this->derivatives;
  }
}

<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\Plugin\Core\Entity\DrupalBookingUnitType.
 */

namespace Drupal\drupalbooking_unit\Plugin\Core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines the drupalbooking_unit type entity.
 *
 * @Plugin(
 *   id = "drupalbooking_unit_type",
 *   label = @Translation("Drupal booking unit type"),
 *   module = "drupalbooking_unit",
 *   controller_class = "Drupal\drupalbooking_unit\DrupalBookingUnitTypeStorageController",
 *   list_controller_class = "Drupal\drupalbooking_unit\DrupalBookingUnitTypeListController",
 *   form_controller_class = {
 *     "default" = "Drupal\drupalbooking_unit\DrupalBookingUnitTypeFormController"
 *   },
 *   config_prefix = "drupalbooking_unit.type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class DrupalBookingUnitType extends ConfigEntityBase {

  /**
   * The drupalbooking_unit type ID.
   *
   * @var string
   */
  public $id;

  /**
   * The drupalbooking_unit type UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The drupalbooking_unit type label.
   *
   * @var string
   */
  public $label;

  /**
   * The default revision setting for drupalbooking_units of this type.
   *
   * @var bool
   */
  public $revision;

  /**
   * The description of the bookable unit type.
   *
   * @var string
   */
  public $description;

  /**
   * Overrides \Drupal\Core\Entity\Entity::uri().
   */
  public function uri() {
    return array(
      'path' => 'admin/drupalbooking/unit-types/manage/' . $this->id(),
      'options' => array(
        'entity_type' => $this->entityType,
        'entity' => $this,
      )
    );
  }
}

<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\DrupalBookingUnitAccessController.
 */

namespace Drupal\drupalbooking_unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\user\Plugin\Core\Entity\User;
use Drupal\Core\Entity\EntityAccessControllerInterface;

/**
 * Defines the access controller for the drupalbooking_unit entity type.
 */
class DrupalBookingUnitAccessController implements EntityAccessControllerInterface {

  /**
   * Implements EntityAccessControllerInterface::viewAccess().
   */
  public function viewAccess(EntityInterface $entity, $langcode = LANGUAGE_DEFAULT, User $account = NULL) {
    return TRUE;
  }

  /**
   * Implements EntityAccessControllerInterface::createAccess().
   */
  public function createAccess(EntityInterface $entity, $langcode = LANGUAGE_DEFAULT, User $account = NULL) {
    return user_access('administer bookings', $account);
  }

  /**
   * Implements EntityAccessControllerInterface::updateAccess().
   */
  public function updateAccess(EntityInterface $entity, $langcode = LANGUAGE_DEFAULT, User $account = NULL) {
    return user_access('administer bookings', $account);
  }

  /**
   * Implements EntityAccessControllerInterface::deleteAccess().
   */
  public function deleteAccess(EntityInterface $entity, $langcode = LANGUAGE_DEFAULT, User $account = NULL) {
    return user_access('administer bookings', $account);
  }

}

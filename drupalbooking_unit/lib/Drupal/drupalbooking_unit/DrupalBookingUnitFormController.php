<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\DrupalBookingUnitFormController.
 */

namespace Drupal\drupalbooking_unit;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFormControllerNG;

/**
 * Form controller for the drupalbooking_unit edit forms.
 */
class DrupalBookingUnitFormController extends EntityFormControllerNG {

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::prepareEntity().
   *
   * Prepares the drupalbooking_unit object.
   *
   * Fills in a few default values, and then invokes hook_drupalbooking_unit_prepare()
   * on all modules.
   */
  protected function prepareEntity(EntityInterface $unit) {
    // Set up default values, if required.
    $unit_type = entity_load('drupalbooking_unit_type', $unit->type->value);
    // If this is a new drupalbooking_unit, fill in the default values.
    if (isset($unit->id->value)) {
      $unit->set('log', NULL);
    }
    // Always use the default revision setting.
    $unit->setNewRevision($unit_type->revision);

    module_invoke_all('drupalbooking_unit_prepare', $unit);
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state, EntityInterface $unit) {
    // Override the default CSS class name, since the user-defined drupalbooking_unit
    // type name in 'TYPE-unit-form' potentially clashes with third-party class
    // names.
    $form['#attributes']['class'][0] = drupal_html_class('unit-' . $unit->type->value . '-form');

    // Basic drupalbooking_unit namermation.
    // These elements are just values so they are not even sent to the client.
    foreach (array('revision_id', 'id') as $key) {
      $form[$key] = array(
        '#type' => 'value',
        '#value' => $unit->$key->value,
      );
    }

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#required' => TRUE,
      '#default_value' => $unit->name->value,
      '#weight' => -5,
      '#description' => t('A brief description of your unit. Used on the <a href="@overview">Blocks administration page</a>.', array('@overview' => url('admin/drupalbooking/units'))),
    );

    $language_configuration = module_invoke('language', 'get_default_configuration', 'drupalbooking_unit', $unit->type->value);

    // Set the correct default language.
    if ($unit->isNew() && !empty($language_configuration['langcode'])) {
      $language_default = language($language_configuration['langcode']);
      $unit->langcode->value = $language_default->langcode;
    }

    $form['langcode'] = array(
      '#title' => t('Language'),
      '#type' => 'language_select',
      '#default_value' => $unit->langcode->value,
      '#languages' => LANGUAGE_ALL,
      '#access' => isset($language_configuration['language_show']) && $language_configuration['language_show'],
    );

    return parent::form($form, $form_state, $unit);
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::submit().
   *
   * Updates the drupalbooking_unit object by processing the submitted values.
   *
   * This function can be called by a "Next" button of a wizard to update the
   * form state's entity with the current step's values before proceeding to the
   * next step.
   */
  public function submit(array $form, array &$form_state) {
    // Build the unit object from the submitted values.
    $unit = parent::submit($form, $form_state);

    // Save as a new revision if requested to do so.
    if (!empty($form_state['values']['revision'])) {
      $unit->setNewRevision();
    }

    return $unit;
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $unit = $this->getEntity($form_state);
    $insert = empty($unit->id->value);
    $unit->save();
    $watchdog_args = array('@type' => $unit->bundle(), '%name' => $unit->label());
    $unit_type = entity_load('drupalbooking_unit_type', $unit->type->value);
    $t_args = array('@type' => $unit_type->label(), '%name' => $unit->label());

    if ($insert) {
      watchdog('content', '@type: added %name.', $watchdog_args, WATCHDOG_NOTICE);
      drupal_set_message(t('@type %name has been created.', $t_args));
    }
    else {
      watchdog('content', '@type: updated %name.', $watchdog_args, WATCHDOG_NOTICE);
      drupal_set_message(t('@type %name has been updated.', $t_args));
    }

    if ($unit->id->value) {
      $form_state['values']['id'] = $unit->id->value;
      $form_state['id'] = $unit->id->value;
      if ($insert) {
        if ($theme = $unit->getTheme()) {
          $form_state['redirect'] = 'admin/drupalbooking/units';
        }
        else {
          $form_state['redirect'] = 'admin/drupalbooking/units';
        }
      }
      else {
        $form_state['redirect'] = 'admin/drupalbooking/units';
      }
    }
    else {
      // In the unlikely case something went wrong on save, the unit will be
      // rebuilt and unit form redisplayed.
      drupal_set_message(t('The unit could not be saved.'), 'error');
      $form_state['rebuild'] = TRUE;
    }

    // Clear the page and unit caches.
    cache_invalidate_tags(array('content' => TRUE));
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::delete().
   */
  public function delete(array $form, array &$form_state) {
    $destination = array();
    if (isset($_GET['destination'])) {
      $destination = drupal_get_destination();
      unset($_GET['destination']);
    }
    $unit = $this->buildEntity($form, $form_state);
    $form_state['redirect'] = array('unit/' . $unit->uuid->value . '/delete', array('query' => $destination));
  }

}

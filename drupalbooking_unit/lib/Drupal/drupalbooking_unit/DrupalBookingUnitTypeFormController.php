<?php

/**
 * @file
 * Contains \Drupal\drupalbooking_unit\DrupalBookingUnitTypeFormController.
 */

namespace Drupal\drupalbooking_unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFormController;

/**
 * Base form controller for category edit forms.
 */
class DrupalBookingUnitTypeFormController extends EntityFormController {

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state, EntityInterface $unit_type) {
    $form = parent::form($form, $form_state, $unit_type);

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $unit_type->label(),
      '#description' => t("Provide a label for this bookable unit type to help identify it in the administration pages."),
      '#required' => TRUE,
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $unit_type->id(),
      '#machine_name' => array(
        'exists' => 'drupalbooking_unit_type_load',
      ),
      '#disabled' => !$unit_type->isNew(),
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#default_value' => $unit_type->description,
      '#description' => t('Enter a description for this bookable unit type.'),
      '#title' => t('Description'),
    );

    $form['revision'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create new revision'),
      '#default_value' => $unit_type->revision,
      '#description' => t('Create a new revision by default for this bookable unit type.')
    );

    if (module_exists('translation_entity')) {
      $form['language'] = array(
        '#type' => 'details',
        '#title' => t('Language settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#group' => 'additional_settings',
      );

      $language_configuration = language_get_default_configuration('drupalbooking_unit', $unit_type->id());
      $form['language']['language_configuration'] = array(
        '#type' => 'language_configuration',
        '#entity_information' => array(
          'entity_type' => 'drupalbooking_unit',
          'bundle' => $unit_type->id(),
        ),
        '#default_value' => $language_configuration,
      );

      $form['#submit'][] = 'language_configuration_element_submit';
    }

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save unit type'),
    );

    return $form;
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $unit_type = $this->getEntity($form_state);
    $status = $unit_type->save();

    $uri = $unit_type->uri();
    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('Drupal booking unit type %label has been updated.', array('%label' => $unit_type->label())));
      watchdog('drupalbooking_unit', 'Drupal booking unit type %label has been updated.', array('%label' => $unit_type->label()), WATCHDOG_NOTICE, l(t('Edit'), $uri['path'] . '/edit'));
    }
    else {
      drupal_set_message(t('Drupal booking unit type %label has been added.', array('%label' => $unit_type->label())));
      watchdog('drupalbooking_unit', 'Drupal booking unit type %label has been added.', array('%label' => $unit_type->label()), WATCHDOG_NOTICE, l(t('Edit'), $uri['path'] . '/edit'));
    }

    $form_state['redirect'] = 'admin/drupalbooking/unit-types';
  }

  /**
   * Overrides \Drupal\Core\Entity\EntityFormController::delete().
   */
  public function delete(array $form, array &$form_state) {
    $unit_type = $this->getEntity($form_state);
    $form_state['redirect'] = 'admin/drupalbooking/unit-types/manage/' . $unit_type->id() . '/delete';
  }

}
